from ..common import cur_pck_route, get_dir_content, clean_path
from flask import send_from_directory, Blueprint
from importlib import import_module
from copy import deepcopy
from os import getcwd

def start(app, routes, cwd=getcwd()):
    '''Function that initialize the static files server

    Main parameters:
     - app. Flask main app object
     - routes. Dictionary that selects which static routes must be initialized. {'static_router_dir':'prefix'} Ex. {'srv/static/files':'static'}

    Optional parameters:
     - cwd. Current working directory. By default it will be the global cwd of the script.
    '''
    if(type(routes) != type({})):
        return False

    def make_return(files_wd):
        return lambda path: send_from_directory(files_wd, path)

    routes_en = []
    for route_dir, route_prefix in routes.items():
        if(type(route_prefix) != type("") or type(route_dir) != type("")):
            print("ERROR. Not valid route \"{}\" @ \"{}\".".format(route_prefix, route_dir))
            continue
        if(route_prefix in routes_en):
            print("ERROR. Repeated route \"{}\" @ \"{}\".".format(route_prefix, route_dir))
            continue
            
        try:
            route_dir = clean_path(route_dir)
            if(route_dir[0] == '/'):
                files_wd = route_dir
            else:
                files_wd = cwd+'/'+route_dir
                files_wd = clean_path(files_wd)

            route_name = 'static_{}'.format('_'.join(route_prefix.split('/')))
            app.add_url_rule('/{}/<path:path>'.format(route_prefix), route_name, make_return(files_wd))

            routes_en.append(route_prefix)
            print("INFO. Static route \"{}\" @ \"{}\" enable.".format(route_prefix, route_dir))
        except Exception as e:
            print(e)
            print("ERROR. Unknown error while creating static route \"{}\" @ \"{}\".".format(route_prefix, route_dir))

    return True