from .db import create_main_db, getDict, quick_query
from flask_restful import Resource
from flask import request

import sqlite3
import time


class devices(Resource):
    def get(self):
        # marshal
        if(request.args.get('extended') != None):
            return quick_query(self.db_file, 'devices_detail')
        else:
            return quick_query(self.db_file, 'devices'), quick_query(self.db_file, 'devices')
    
    def __init__(self, **kwargs):
        self.db_file = kwargs['db_file']