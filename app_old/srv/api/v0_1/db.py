from ...common import array2dict, cur_pck_route, getfwd
import sqlite3
import json
import os

def create_main_db(db_file):
    import time
    db = sqlite3.connect(db_file)

    cur_tm = int(time.time())

    db.cursor()
    db.execute('''CREATE TABLE "devices"
                  ("ID"	INTEGER PRIMARY KEY AUTOINCREMENT,
                   "sn"	TEXT DEFAULT 'A000000',
                   "name"	TEXT,
                   "description"	TEXT,
                   "creation_time",
                   "last_edit_time",
                   "last_ping_time",
                   "creation_user"	TEXT DEFAULT 'SYSTEM',
                   "location"	TEXT,
                   "enable"	INTEGER DEFAULT '0',
                   "fix"	INTEGER DEFAULT '-1',
                   "allow_all"	INTEGER DEFAULT '0',
                   "allow_all_range"	TEXT,
                   "open_request"	INTEGER DEFAULT '0',
                   "master_pin"	INTEGER DEFAULT '000000',
                   "master_phone"	TEXT,
                   "internal_phone"	TEXT,
                   "groups"	TEXT,
                   "comment"	TEXT,
                   "extra_text"	TEXT,
                   "extra_blob"	BLOB
                  );''')

    db.execute('''INSERT INTO "main"."devices"
                  ("ID","name","description","creation_time","last_edit_time","last_ping_time","creation_user","location","allow_all_range","master_phone","internal_phone","groups","comment","extra_text","extra_blob")
                  VALUES
                  (NULL,'DEFAULT_DEV','DEFAULT CONFIG','{}','{}','{}',NULL,'NONE','0',NULL,NULL,NULL,NULL,NULL,NULL);
               '''.format(cur_tm, cur_tm, cur_tm))
    
    db.execute('''CREATE TABLE "users_default"
                  ("ID"	INTEGER PRIMARY KEY AUTOINCREMENT,
                   "ID_device"	INTEGER DEFAULT '1',
                   "phone"	TEXT,
                   "block"	INTEGER DEFAULT '0',
                   "fix"	INTEGER DEFAULT '0',
                   "fix_times"	INTEGER,    
                   "num_access"	INTEGER DEFAULT '0',
                   "enter_time"	INTEGER,
                   "exit_time"	INTEGER,
                   "last_access"	INTEGER,
                   "delay_time"	INTEGER DEFAULT '500',
                   "relay_on_time"	INTEGER DEFAULT '5000',
                   "allow_all_enable"	INTEGER DEFAULT '0',
                   "comment"	TEXT,
                   "extra_text"	TEXT,
                   "extra_blob"	BLOB,
                   FOREIGN KEY("ID_device") REFERENCES "devices"("ID")
                  );''')

    db.execute('''INSERT INTO "main"."users_default"
                  ("ID","ID_device","phone","fix_times","enter_time","exit_time","last_access","comment","extra_text","extra_blob")
                  VALUES
                  (NULL,'1',NULL,'-1',NULL,NULL,NULL,NULL,NULL,NULL);

               ''')

    db.commit()
    db.close()


def get_table_info(db, table):
   dbc = db.cursor()
   try:
      dbc.execute("PRAGMA table_info({});".format(table))
      names_lst = [i[1] for i in dbc.fetchall()]
   except:
      print("ERROR. Not valid DB query in table {}".format(table))
      return None

   return names_lst


def getDict(db, table, c_names=None):
   dbc = db.cursor()
   if(type(c_names) == type("")):
      names = c_names
      names_lst = [i.strip() for i in c_names]
   elif(type(c_names) == type([])):
      names = ','.join(c_names)
      names_lst = c_names
   else:
      names = '*'
      names_lst = get_table_info(db, table)

   try:
      dbc.execute("SELECT {} FROM {}".format(names, table))
      data = dbc.fetchall()
   except:
      print("ERROR. Not valid DB query. {} in {}".format(names, table))
      return None
   data2 = []
   for i in data:
      data2.append(array2dict(i, names_lst))

   return {table:data2}


def quick_query(db_file, qType, json_file="quick_query.json"):
   try:
      with open(getfwd(__file__)+'/'+json_file) as f:
         available = json.loads(f.read())
   except:
      return []


   db = sqlite3.connect(db_file)

   if(available[qType][1] == '*'):
      c_names = get_table_info(db, available[qType][0])
   else:
      c_names = available[qType][1]

   data = getDict(db, available[qType][0], c_names=c_names)
   db.close()

   return data