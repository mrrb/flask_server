from flask_restful import reqparse, abort, Api, Resource
from ...common import clean_path
from .db import create_main_db
from flask import Blueprint
from copy import deepcopy

import os


db_list = {'main' : 'DB_main_v0_1.db'}


def init_api(prefix = __loader__.name.split('.')[-2],
             name   = __loader__.name.split('.')[-2],
             db_dir = "db",
             cwd    = os.getcwd()):
    '''Function that initialize the current API.
    
    Optional parameters:
     - prefix. Text in the URL before the API content. Ex. (prefix="/api/v1") test.page/api/v1
     - name. Internal name of the api.
     - db_dir. Location of the sqlite3 DBs.
     - cwd. Current Working Directory where all the files are located.
    '''

    # If the DB folder does not exist, must be created first.
    db_dir = clean_path(db_dir)
    if(db_dir[0] == '/'):
        db_wd = db_dir
    else:
        db_wd = cwd+'/'+db_dir
        db_wd = clean_path(db_wd)
    if(not os.path.exists(db_wd)):
        os.makedirs(db_wd)

    # Every DB file is checked, and if It doesn't exist, It will be created with the default values.
    db_list_file = {}
    for i,j in db_list.items():
        db_file = db_wd+'/'+j
        db_list_file[i] = db_file
        # print(db_file)
        if(not os.path.exists(db_file)):
            print("WARNING. No DB {} file found. Creating one with default values.".format(i))
            create_main_db(db_file)

    # The blueprint for the current API is created
    pr = '/'+'/'.join(list(filter(('').__ne__, prefix.split('/'))))
    # pr = '/'+'/'.join(list(filter(lambda x: x != '', prefix.split('/'))))
    api_bp = Blueprint(name, __name__, url_prefix=pr)
    api = Api(api_bp)
 
    # API routes
    from .api_devices import devices

    api.add_resource(devices, '/devices', resource_class_kwargs={'db_file':db_list_file['main']})

    return api_bp

# return data, 200, {'test':12}