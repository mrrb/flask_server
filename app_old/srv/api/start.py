from importlib import import_module
from ..common import cur_pck_route, get_dir_content
from os import getcwd

def gen_api_var(cwd=getcwd()):
    dirs = get_dir_content(path=cwd+'/'+cur_pck_route(__loader__, separator_out='/'), remove_hidden=['_', '.', '~'])[0]
    return dict(zip(dirs['dir'], dirs['dir']))

def start(app, APIs=None, cwd=getcwd()):
    '''Function that initialize the API server

    Main parameters:
     - app. Flask main app object

    Optional parameters:
     - APIs. Dictionary that selects which API must be initialized. It will initialize all the available APIs with its folder names by default. {'folder_name':'prefix'} Ex. {'v0_1':'v01'}
     - cwd. Current working directory. By default it will be the global cwd of the script.
    '''
    if(APIs == None or type(APIs) != type({})):
        APIs = gen_api_var(cwd=cwd)

    from flask_restful import Api

    api = Api(app)

    api_pcks = {}
    api_bps = {}
    for api_dir, api_name in APIs.items():
        if(type(api_name) != type("") or type(api_dir) != type("")):
            print("ERROR. Not valid api name \"{}\"".format(api_name))
            continue
        if(api_name in api_pcks.keys()):
            print("ERROR. Repeated api \"{}\"".format(api_name))
            continue
            
        try:
            api_pcks[api_name] = import_module(cur_pck_route(__loader__, separator_out='.')+'.'+api_dir)
            api_bps[api_name]  = api_pcks[api_name].init_api(prefix='/api/'+api_name, cwd=cwd)
            app.register_blueprint(api_bps[api_name])
            print("INFO. API \"{}\" enable.".format(api_name))
        except ImportError:
            print("ERROR. ImportError while loading api {}.".format(api_name))
        except Exception as e:
            print(e)
            print("ERROR. Unknown error while loading api {}.".format(api_name))

    return api