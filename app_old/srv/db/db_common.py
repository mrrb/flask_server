import sqlite3

# Main DB class.
class db:
    available_db_types = {"sqlite3":0}
    __type = ""
    __connected = False
    db = None

    def __init__(self, db_type = "sqlite3"):
        if(db_type in self.available_db_types.keys()):
            self.__type = self.available_db_types[db_type]
        else:
            raise TypeError("Not valid DB type.")

    # Depending on the db type used, this function will create a connection to the DB
    def connect(self, route):
        if(self.__type == 0):
            self.db = sqlite3.connect(route)

    def isConnected(self):
        if(self.__connected == True):
            return True
        else:
            return False
