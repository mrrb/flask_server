import os

def cur_pck_route(rt=__loader__, separator_in='.', separator_out='/', include_self=True):
    '''Function that returns the current relative route of a package. If no parameter is given, it will return the relative route of this library.
    Recommended use: cur_pck_route(__loader__).

    Optional parameters.
     - rt. Package loader variable (__loader__)
     - separator_in. Character that separate each element of the input.
     - separator_out. Output separator character for the route.
     - include_self. If False, the last element in rt will not be displayed.
    '''
    try:
        route_list = rt.name.split(separator_in)[:-1]
        if(not include_self):
            route_list = route_list[:-1]
            
        return separator_out.join(route_list)
    except:
        return ""


def getfwd(file_var=__file__):
    '''Get File Working Directory

    Returns the working directory of a python script
    '''
    return os.path.dirname(os.path.abspath(file_var))


def get_dir_content(get_type="all", path='.', recursive=False, remove_hidden=None):
    '''Function that returns a list with the content of all the files and/or directories in a given path.

    The base path where you want to obtain its contents is given by the optional path parameter.

    There are 3 types of returns:
      1 - get_type='all'. Returns folders, files and path.
      2 - get_type='dir'. Returns only folders and path.
      3 - get_type='file'. Returns only files and path.

    If you want to search recursively, recursive must be True.

    If you don't want to obtain hidden files/folders, pass a list with the initial characters that make them hide.
    '''
    from os import walk

    out = []
    try:
        get_type = get_type.lower()
        if(not(get_type in ['all', 'dir', 'file'])):
            raise Exception('Not valid type!')
        for (dirpath, dirnames, filenames) in walk(path):
            try:
                [dirnames.pop(i) if j[0] in remove_hidden else None for i,j in enumerate(dirnames)]
                [filenames.pop(i) if j[0] in remove_hidden else None for i,j in enumerate(filenames)]
            except:
                print(1)

            temp_dict = {}
            temp_dict['path'] = dirpath
            if(get_type == 'dir' or get_type == 'all'):
                temp_dict['dir'] = dirnames
            if(get_type == 'file' or get_type == 'all'):
                temp_dict['file'] = filenames
            out.append(temp_dict)
            if(not(recursive)):
                break
        return out
    except:
        print("ERROR. Invalid input parameters.")
        return []


# ToDo
def clean_path(path):
    pre = ''
    if(path[0] == '/'):
        pre = '/'
        
    return pre + '/'.join(list(filter(('').__ne__, path.split('/'))))


def array2dict(arr, names=None):
    temp = {}
    for i in range(len(arr)):
        try:
            temp[names[i]] = arr[i]
        except:
            temp[str(i)] = arr[i]

    return temp