from flask import Blueprint, render_template
from ...common import clean_path
import os

def init_view(prefix = __loader__.name.split('.')[-1],
              name   = __loader__.name.split('.')[-1],
              db_dir = "db",
              cwd    = os.getcwd(),
              wip    = False):
    '''Function that initialize the current web view.
    
    Optional parameters:
     - prefix. Text in the URL before the API content. Ex. (prefix="/api/v1") test.page/api/v1
     - name. Internal name of the api.
     - db_dir. Location of the sqlite3 DBs.
     - cwd. Current Working Directory where all the files are located.
    '''
    # If the DB folder does not exist, must be created first.
    db_dir = clean_path(db_dir)
    if(db_dir[0] == '/'):
        db_wd = db_dir
    else:
        db_wd = cwd+'/'+db_dir
        db_wd = clean_path(db_wd)
    if(not os.path.exists(db_wd)):
        os.makedirs(db_wd)

    # The blueprint for the current view is created
    pr = '/'+'/'.join(list(filter(('').__ne__, prefix.split('/'))))
    web_bp = Blueprint(name, __name__, url_prefix=pr)

    # @web_bp.route('/', defaults={'path': ''})
    @web_bp.route('/s/<path:path>')
    def index(path):
        return render_template("wip/wip.html")


    return web_bp