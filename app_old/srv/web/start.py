from flask import Blueprint, render_template, send_from_directory
from ..common import get_dir_content, cur_pck_route, clean_path
from importlib import import_module
from os import getcwd


def gen_web_var(cwd=getcwd):
    dirs = get_dir_content(path=cwd+'/'+cur_pck_route(__loader__, separator_out='/')+'/views', remove_hidden=['_', '.', '~'])[0]
    dirs['file'] = list(filter(lambda x: x[-3:] == '.py', dirs['file'])) 
    return dict(zip(dirs['file'], [i[:-3] for i in dirs['file']]))

def start(app, WEBs=None, cwd=getcwd(), wip = None):
    '''Function that initialize the web server

    Main parameters:
     - app. Flask main app object

    Optional parameters:
     - WEBs. Dictionary that selects which WEB must be initialized. It will initialize all the available WEBs in the views folder by default. {'file_name in views folder':'prefix'} Ex. {'wip':'wip_test'}
     - cwd. Current working directory. By default it will be the global cwd of the script.
    '''
    if(WEBs == None or type(WEBs) != type({})):
        WEBs = gen_web_var(cwd=cwd)

    web_views = {}
    web_bps = {}
    for view_file, view_name in WEBs.items():
        if(type(view_name) != type("") or type(view_file) != type("")):
            print("ERROR. Not valid view name \"{}\" @ \"{}.py\"".format(view_name, view_file))
            continue
        if(view_name in web_views.keys()):
            print("ERROR. Repeated view \"{}\" @ \"{}.py\"".format(view_name, view_file))
            continue
            
        try:
            wip_temp = False
            if(view_file == 'wip'):
                wip_temp == True
            web_views[view_name] = import_module(cur_pck_route(__loader__, separator_out='.')+'.views.'+view_file)
            web_bps[view_name]   = web_views[view_name].init_view(prefix='/'+view_name, cwd=cwd, wip=wip_temp)
            app.register_blueprint(web_bps[view_name])
            print("INFO. Web view \"{}\" @ \"{}.py\" enable.".format(view_name, view_file))
        except ImportError:
            print("ERROR. ImportError while loading web view \"{}\" @ \"{}.py\".".format(view_name, view_file))
        except Exception as e:
            print(e)
            print("ERROR. Unknown error while loading web view \"{}\" @ \"{}.py\".".format(view_name, view_file))

    return True