#!../venv/bin/python
from srv.static import start as srv_static
from srv.api import start as srv_api
from srv.web import start as srv_web
from srv.common import getfwd
from config import Config
from flask import Flask
from sys import argv
import sqlite3

app = Flask(__name__, template_folder="srv/web/templates", static_url_path='')
app.config.from_object(Config)

if __name__ == '__main__':
    cwd = getfwd(__file__)

    srv_api.start(app,
                  APIs = {"v0_1":"v01"},
                  cwd  = cwd)
    srv_static.start(app,
                     {'srv/web/assets':'assets',
                      'srv/static/files':'static'})
    srv_web.start(app,
                  WEBs = {"wip":""},
                  cwd  = cwd,
                  wip  = "wip")

    app.run(debug=True)