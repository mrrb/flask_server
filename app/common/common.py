import os

def cur_pck_route(rt=__loader__, separator_in='.', separator_out='/', include_self=True):
    '''Returns the current relative route of a package. If no parameter is given, it will return the relative route of this library.
    Recommended use: cur_pck_route(__loader__).

    Optional parameters.
     - rt. Package loader variable (__loader__)
     - separator_in. Character that separate each element of the input.
     - separator_out. Output separator character for the route.
     - include_self. If False, the last element in the rt route will not be displayed.
    '''
    assert (type(separator_in)  != str), "Invalid separator_in value type!"
    assert (type(separator_out) != str), "Invalid separator_out value type!"

    try:
        route_list = rt.name.split(separator_in)[:-1]
        if(not include_self):
            route_list = route_list[:-1]
            
        return separator_out.join(route_list)
    except:
        return ""


def getfwd(file_var=__file__):
    '''Get File Working Directory

    Returns the working directory of a python script.
    '''
    return os.path.dirname(os.path.abspath(file_var))


def get_dir_content(get_type="all", path='.', recursive=False, remove_hidden=None):
    '''Returns a list containing all the files and/or directories in a given path.

    The base path where you want to obtain its contents is given by the optional path parameter.

    There are 3 types of returns:
      1 - get_type='all'. Returns folders, files and path. [Default]
      2 - get_type='dir'. Returns only folders and path.
      3 - get_type='file'. Returns only files and path.

    If you want to search recursively, 'recursive' must be True.

    If you don't want to obtain hidden files/folders, pass a list with the initial characters that make them hide.
    '''
    from os import walk

    out = []
    try:
        get_type = get_type.lower()
        if(not(get_type in ['all', 'dir', 'file'])):
            raise Exception('Not valid type!')
        for (dirpath, dirnames, filenames) in walk(path):
            try:
                [dirnames.pop(i) if j[0] in remove_hidden else None for i,j in enumerate(dirnames)]
                [filenames.pop(i) if j[0] in remove_hidden else None for i,j in enumerate(filenames)]
            except:
                print(1)

            temp_dict = {}
            temp_dict['path'] = dirpath
            if(get_type == 'dir' or get_type == 'all'):
                temp_dict['dir'] = dirnames
            if(get_type == 'file' or get_type == 'all'):
                temp_dict['file'] = filenames
            out.append(temp_dict)
            if(not(recursive)):
                break
        return out
    except:
        raise Exception("ERROR. Invalid input parameters.")


def clean_path(path, separator_in='/', separator_out='/'):
    '''Returns a cleaner file/folder path that the one given as a parameter

    Ex. Input:  './//foo//bar//'
        Output: './foo/bar/'
    '''

    pre = ''
    if(path[0] == separator_in):
        pre = separator_out
        
    out = pre + separator_out.join(list(filter(('').__ne__, path.split(separator_in))))

    if(separator_in != separator_out):
        return clean_path(out, separator_in=separator_out, separator_out=separator_out)
    else:
        return out


def array2dict(arr, keys=None):
    '''Converts an array into a dictionary with incremental numerical key

    Custom dictionary keys can be used if an array containing them is passed as optional parameter.
    '''
    temp = {}
    for i in range(len(arr)):
        try:
            temp[keys[i]] = arr[i]
        except:
            temp[str(i)] = arr[i]

    return temp